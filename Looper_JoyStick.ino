const int joy_btn = 2;
const int led_con= 3;
const int btn_A = 4;
const int x_joy = 6;
const int y_joy = 7;
const int btn_B = 5;
const int btn_C = 8;
const int btn_D = 9;
void setup() {
  Serial.begin(38400);
  pinMode(joy_btn,INPUT);
  pinMode(led_con,OUTPUT);
  pinMode(btn_A,INPUT);
  pinMode(btn_B,INPUT);
  pinMode(btn_C,INPUT);
  pinMode(btn_D,INPUT);
  digitalWrite(joy_btn,HIGH);
}
void loop() {
  if(Serial.available()){
    digitalWrite(led_con,HIGH);
    }

    String axis1 =String(map(analogRead(x_joy),0,1023,0,9));
    String axis2 = String(map(analogRead(y_joy),0,1023,0,9));
    String btt1 = String(digitalRead(joy_btn));
    String btt2 = String(digitalRead(btn_A));
    String btt3 = String(digitalRead(btn_B));
    String btt4 = String(digitalRead(btn_C));
    String btt5 = String(digitalRead(btn_D));
    String data =  axis1 + axis2 + btt1 + btt2 + btt3 + btt4 + btt5;
    delay(10);
    Serial.println(data);
    Serial.flush();
}

